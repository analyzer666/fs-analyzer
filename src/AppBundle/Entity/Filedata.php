<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Таблица с данными о файлах
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class Filedata
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * Имя файла
     *
     * @ORM\Column(type="string")
     */
    protected $imya;

    /**
     * Путь к файлу
     *
     * @ORM\Column(type="string")
     */
    protected $put;

    /**
     * Тип - файл или папка
     *
     * @ORM\Column(type="string")
     */
    protected $tip;

    /**
     * Размер
     *
     * @ORM\Column(type="bigint")
     */
    protected $razmer;

    /**
     * Атрибуты
     *
     * @ORM\Column(type="string")
     */
    protected $atributy;

    /**
     * Время создания
     *
     * @ORM\Column(type="datetime")
     */
    protected $vremyasozd;

    /**
     * Время открытия
     *
     * @ORM\Column(type="datetime")
     */
    protected $vremyaotk;

    /**
     * Время изменения
     *
     * @ORM\Column(type="datetime")
     */
    protected $vremyaizm;

    /**
     * Имя компа
     *
     * @ORM\Column(type="string")
     */
    protected $imyakomp;

    /**
     * Имя компа
     *
     * @ORM\Column(type="string")
     */
    protected $vladelets;

    public function __toString() {
        return $this->imya;
    }

    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imya
     *
     * @param string $imya
     *
     * @return Filedata
     */
    public function setImya($imya)
    {
        $this->imya = $imya;

        return $this;
    }

    /**
     * Get imya
     *
     * @return string
     */
    public function getImya()
    {
        return $this->imya;
    }

    /**
     * Set put
     *
     * @param string $put
     *
     * @return Filedata
     */
    public function setPut($put)
    {
        $this->put = $put;

        return $this;
    }

    /**
     * Get put
     *
     * @return string
     */
    public function getPut()
    {
        return $this->put;
    }

    /**
     * Set tip
     *
     * @param string $tip
     *
     * @return Filedata
     */
    public function setTip($tip)
    {
        $this->tip = $tip;

        return $this;
    }

    /**
     * Get tip
     *
     * @return string
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * Set razmer
     *
     * @param integer $razmer
     *
     * @return Filedata
     */
    public function setRazmer($razmer)
    {
        $this->razmer = $razmer;

        return $this;
    }

    /**
     * Get razmer
     *
     * @return integer
     */
    public function getRazmer()
    {
        return $this->razmer;
    }

    /**
     * Set atributy
     *
     * @param integer $atributy
     *
     * @return Filedata
     */
    public function setAtributy($atributy)
    {
        $this->atributy = $atributy;

        return $this;
    }

    /**
     * Get atributy
     *
     * @return integer
     */
    public function getAtributy()
    {
        return $this->atributy;
    }

    /**
     * Set vremyasozd
     *
     * @param \DateTime $vremyasozd
     *
     * @return Filedata
     */
    public function setVremyasozd($vremyasozd)
    {
        $this->vremyasozd = $vremyasozd;

        return $this;
    }

    /**
     * Get vremyasozd
     *
     * @return \DateTime
     */
    public function getVremyasozd()
    {
        return $this->vremyasozd;
    }

    /**
     * Set vremyaotk
     *
     * @param \DateTime $vremyaotk
     *
     * @return Filedata
     */
    public function setVremyaotk($vremyaotk)
    {
        $this->vremyaotk = $vremyaotk;

        return $this;
    }

    /**
     * Get vremyaotk
     *
     * @return \DateTime
     */
    public function getVremyaotk()
    {
        return $this->vremyaotk;
    }

    /**
     * Set vremyaizm
     *
     * @param \DateTime $vremyaizm
     *
     * @return Filedata
     */
    public function setVremyaizm($vremyaizm)
    {
        $this->vremyaizm = $vremyaizm;

        return $this;
    }

    /**
     * Get vremyaizm
     *
     * @return \DateTime
     */
    public function getVremyaizm()
    {
        return $this->vremyaizm;
    }

    /**
     * Set imyakomp
     *
     * @param string $imyakomp
     *
     * @return Filedata
     */
    public function setImyakomp($imyakomp)
    {
        $this->imyakomp = $imyakomp;

        return $this;
    }

    /**
     * Get imyakomp
     *
     * @return string
     */
    public function getImyakomp()
    {
        return $this->imyakomp;
    }

    /**
     * Set vladelets
     *
     * @param string $vladelets
     *
     * @return Filedata
     */
    public function setVladelets($vladelets)
    {
        $this->vladelets = $vladelets;
        return $this;
    }

    /**
     * Get vladelets
     *
     * @return string
     */
    public function getVladelets()
    {
        return $this->vladelets;
    }
}
